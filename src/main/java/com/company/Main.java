package com.company;

import java.util.Arrays;
import java.util.List;

public class Main {

    public static void main(String[] args) {
        
        Rate r1 = new Rate(254, 4.85);
        Rate r2 = new Rate(500, 3.9);
        Rate r3 = new Rate(1, 5);
        //Id: autoIncrement, Orders: 254, Average rate: 4.85;
	    //Id: autoIncrement, Orders: 500, Average rate: 3.9;
	    //Id: autoIncrement, Orders: 1, Average rate: 5.0;


        List<Rate> rates = Arrays.asList(r1, r2, r3);
        rates.sort (Rate.byRate);
        for(int i=rates.size()-1;i>=0;--i) {
            System.out.println(rates.get(i));
        }

        //Output:
        //Id: 2, Average rate: 3.9, Orders: 500, Overall: 1950  (Will be 1st by rates)
        //Id: 1, Average rate: 4.85, Orders: 254, Overall: 1231 (2nd)
        //Id: 3, Average rate: 5.0, Orders: 1, Overall: 5 (3rd)
        //Formula: "Overall = Average rate * Orders"

    }
}
