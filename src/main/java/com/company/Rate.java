package com.company;

import java.util.Comparator;

public class Rate {
    private int orders;
    public int overallRate;
    private double rate;
    private static int id_gen = 1;
    private int id;
    public static Comparator<Rate> byRate = Comparator.comparing(r -> r.getOverallRate());

    private Rate() {
        setId(id);
        setOverallRate(overallRate);
    }

    public Rate(int orders, double rate) {
        this();
        setOrders(orders);
        setRate(rate);
    }

    public void setId(int id) {
        this.id = id_gen++;
    }

    public int getId() {
        return id;
    }

    public int getOrders() {
        return orders;
    }

    public void setOrders(int orders) {
        this.orders = orders;
    }

    public int getOverallRate() {
        return (int) (rate * orders);
    }

    public void setOverallRate(int overallRate) {
        this.overallRate = overallRate;
    }

    public double getRate() {
        return rate;
    }

    public void setRate(double rate) {
        this.rate = rate;
    }

    @Override
    public String toString() {
        return "Id: " + id + ", rate: " + rate + ", orders: " + orders + ", overall: " + getOverallRate() +"\n";
    }
}
